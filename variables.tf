variable "cache_private_objects" {
  type        = bool
  description = "Whether to cache private objects."
  default     = false
}

variable "cloudflare_account_id" {
  type        = string
  description = "Cloudflare account ID."
  sensitive   = true
}

variable "cloudflare_zone_id" {
  type        = string
  description = "Cloudflare zone ID."
}

variable "enabled" {
  type        = bool
  description = "Enable the static cache worker in Cloudflare."
  default     = true
}

variable "elasticsearch_auth" {
  type        = string
  description = "Authorization header value for Elasticsearch."
  sensitive   = true
}

variable "elasticsearch_host" {
  type        = string
  description = "Elasticsearch host."
}

variable "entrypoint" {
  type        = string
  description = "FQDN for the static cache worker."
}

variable "environment" {
  type        = string
  description = "Environment name"
}

variable "external_storage_token" {
  type        = string
  description = "GitLab external storage token."
  sensitive   = true
}

variable "mode" {
  type        = string
  description = "Caching mode: `aggressive` or `conservative`."
  default     = "aggressive"

  validation {
    condition     = contains(["aggressive", "conservative"], var.mode)
    error_message = "The mode value must be either \"aggressive\" or \"conservative\"."
  }
}

variable "origin_host" {
  type        = string
  description = "Origin host."
}

variable "sentry_key" {
  type        = string
  description = "Sentry key."
  sensitive   = true
}

variable "sentry_project_id" {
  type        = string
  description = "Sentry project ID."
}
