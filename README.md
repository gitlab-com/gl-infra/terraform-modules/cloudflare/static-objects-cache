# GitLab.com Static Objects Cache Terraform Module

## What is this?

This module provisions a Cloudflare Worker for a static objects cache.

<!-- BEGIN_TF_DOCS -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 1.0 |
| <a name="requirement_cloudflare"></a> [cloudflare](#requirement\_cloudflare) | >= 4.39.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_cloudflare"></a> [cloudflare](#provider\_cloudflare) | >= 4.39.0 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [cloudflare_record.static-objects-cache-dns-record](https://registry.terraform.io/providers/cloudflare/cloudflare/latest/docs/resources/record) | resource |
| [cloudflare_workers_route.static-objects-cache-route](https://registry.terraform.io/providers/cloudflare/cloudflare/latest/docs/resources/workers_route) | resource |
| [cloudflare_workers_script.static-objects-cache](https://registry.terraform.io/providers/cloudflare/cloudflare/latest/docs/resources/workers_script) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_cache_private_objects"></a> [cache\_private\_objects](#input\_cache\_private\_objects) | Whether to cache private objects. | `bool` | `false` | no |
| <a name="input_cloudflare_account_id"></a> [cloudflare\_account\_id](#input\_cloudflare\_account\_id) | Cloudflare account ID. | `string` | n/a | yes |
| <a name="input_cloudflare_zone_id"></a> [cloudflare\_zone\_id](#input\_cloudflare\_zone\_id) | Cloudflare zone ID. | `string` | n/a | yes |
| <a name="input_elasticsearch_auth"></a> [elasticsearch\_auth](#input\_elasticsearch\_auth) | Authorization header value for Elasticsearch. | `string` | n/a | yes |
| <a name="input_elasticsearch_host"></a> [elasticsearch\_host](#input\_elasticsearch\_host) | Elasticsearch host. | `string` | n/a | yes |
| <a name="input_enabled"></a> [enabled](#input\_enabled) | Enable the static cache worker in Cloudflare. | `bool` | `true` | no |
| <a name="input_entrypoint"></a> [entrypoint](#input\_entrypoint) | FQDN for the static cache worker. | `string` | n/a | yes |
| <a name="input_environment"></a> [environment](#input\_environment) | Environment name | `string` | n/a | yes |
| <a name="input_external_storage_token"></a> [external\_storage\_token](#input\_external\_storage\_token) | GitLab external storage token. | `string` | n/a | yes |
| <a name="input_mode"></a> [mode](#input\_mode) | Caching mode: `aggressive` or `conservative`. | `string` | `"aggressive"` | no |
| <a name="input_origin_host"></a> [origin\_host](#input\_origin\_host) | Origin host. | `string` | n/a | yes |
| <a name="input_sentry_key"></a> [sentry\_key](#input\_sentry\_key) | Sentry key. | `string` | n/a | yes |
| <a name="input_sentry_project_id"></a> [sentry\_project\_id](#input\_sentry\_project\_id) | Sentry project ID. | `string` | n/a | yes |

## Outputs

No outputs.
<!-- END_TF_DOCS -->

## Contributing

Please see [CONTRIBUTING.md](./CONTRIBUTING.md).

## License

See [LICENSE](./LICENSE).
