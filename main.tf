resource "cloudflare_workers_script" "static-objects-cache" {
  account_id = var.cloudflare_account_id
  name       = "${var.environment}-static-objects-cache"

  content = templatefile("${path.module}/scripts/fetch-handler.js.tmpl", {
    elasticsearch_auth     = var.elasticsearch_auth
    elasticsearch_host     = var.elasticsearch_host
    environment            = var.environment
    cache_private_objects  = var.cache_private_objects
    external_storage_token = var.external_storage_token
    mode                   = upper(var.mode)
    origin_host            = var.origin_host
    sentry_key             = var.sentry_key
    sentry_project_id      = var.sentry_project_id
    }
  )
}

resource "cloudflare_workers_route" "static-objects-cache-route" {
  count = var.enabled ? 1 : 0

  zone_id     = var.cloudflare_zone_id
  pattern     = "${var.entrypoint}/*"
  script_name = cloudflare_workers_script.static-objects-cache.name
}

resource "cloudflare_record" "static-objects-cache-dns-record" {
  zone_id = var.cloudflare_zone_id
  name    = var.entrypoint
  content = "192.0.2.1"
  type    = "A"
  proxied = true
}
